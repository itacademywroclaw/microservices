#!/bin/sh
#REMEMBER >>> Enable the Cloud SQL Admin API in your project!
#REMEMBER >>> sudo chmod 755 deploy.sh
PROJECT_ID=$(gcloud config get-value project)
INSTANCE_NAME=$(cat /dev/urandom | tr -dc 'a-za-z' | fold -w 8 | head -n 1)
DB_ROOT_PASS="root123"
DB_NAME="microservices-db"
DB_USER="Zenek"
DB_USER_PASS="Zenkowehaslo"
SPACE=$(cat /dev/urandom | tr -dc 'a-za-z' | fold -w 8 | head -n 1)
CLUSTER_NAME=$(cat /dev/urandom | tr -dc 'a-za-z' | fold -w 8 | head -n 1)
#General settings
gcloud config set project $PROJECT_ID
gcloud config set compute/zone us-central1-a
gcloud config list project
gcloud auth activate-service-account --key-file=key.json
gcloud auth list
#SQL instance deployment
gcloud sql instances create $INSTANCE_NAME --tier=db-g1-small --region=us-central1
gcloud sql instances list
gcloud sql users set-password root \
    --host=% --instance=$INSTANCE_NAME --password "$DB_ROOT_PASS"
#MySQL db deployment
#gcloud sql connect $INSTANCE_NAME --user=root
gcloud sql databases create $DB_NAME --instance=$INSTANCE_NAME
gcloud sql databases list --instance=$INSTANCE_NAME
gcloud sql databases describe $DB_NAME --instance=$INSTANCE_NAME
gcloud sql users create $DB_USER \
   --host=% --instance=$INSTANCE_NAME --password=$DB_USER_PASS        
gcloud sql users list --instance=$INSTANCE_NAME

#Kubernetes cluster deployment
gcloud container clusters create $CLUSTER_NAME
gcloud container clusters get-credentials $CLUSTER_NAME
#kubectl apply --filename=cluster.yaml
#gcloud container clusters list
kubectl get nodes
kubectl create namespace $SPACE
#kubectl config view
#
#gcloud sql instances describe $INSTANCE_NAME | grep 'name\|ipAddres' > instance.txt
#gcloud sql databases describe $DB_NAME --instance=$INSTANCE_NAME
#mysql --host=[INSTANCE_IP] --user=root --password=$DB_ROOT_PASS
#gcloud sql instances describe $INSTANCE_NAME | sed -r '/\n/!s/[0-9.]+/\n&\n/;/^([0-9]{1,3}\.){3}[0-9]{1,3}\n/P;D' | head -n 1



