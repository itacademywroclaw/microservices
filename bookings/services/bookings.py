from tools import root_dir, nice_json
from flask import Flask
import json
from werkzeug.exceptions import NotFound
import collections
import mysql.connector
import os
#import sqlite3

app = Flask(__name__)

#db = mysql.connector.connect(
#    host="146.148.73.26",
#    user=os.environ['SECRET_USERNAME'],
#    passwd=os.environ['SECRET_PASSWORD'],
#    database="micro"
#)

#cur = db.cursor()

#try:
#    cur.execute("CREATE TABLE bookings (id VARCHAR(100), date VARCHAR(20), movies VARCHAR(100))")
#except:
#    cur.execute("INSERT INTO bookings (id, date, movies) VALUES('chris_rivers', '20151201', '267eedb8-0f5d-42d5-8f43-72426b9fb3e6')")
#    cur.execute("INSERT INTO bookings (id, date, movies) VALUES('garret_heaton', '20151201', '267eedb8-0f5d-42d5-8f43-72426b9fb3e6')")
#    cur.execute("INSERT INTO bookings (id, date, movies) VALUES('garret_heaton', '20151202', '276c79ec-a26a-40a6-b3d3-fb242a5947b6')")
#    cur.execute("INSERT INTO bookings (id, date, movies) VALUES('dwight_schrute', '20151201', '7daf7208-be4d-4944-a3ae-c1c2f516f3e6')")
#    cur.execute("INSERT INTO bookings (id, date, movies) VALUES('dwight_schrute', '20151201', '267eedb8-0f5d-42d5-8f43-72426b9fb3e6')")
#    cur.execute("INSERT INTO bookings (id, date, movies) VALUES('dwight_schrute', '20151205', 'a8034f44-aee4-44cf-b32c-74cf452aaaae')")
#    cur.execute("INSERT INTO bookings (id, date, movies) VALUES('dwight_schrute', '20151205', '276c79ec-a26a-40a6-b3d3-fb242a5947b6')")
#    db.commit()


@app.route("/", methods=['GET'])
def hello():
    return nice_json({
        "uri": "/",
        "subresource_uris": {
            "bookings": "/bookings",
            "booking": "/bookings/<username>"
        }
    })


@app.route("/bookings", methods=['GET'])
def booking_list():
    db = mysql.connector.connect(
        host="127.0.0.1",
        user=os.environ['SECRET_USERNAME'],
        passwd=os.environ['SECRET_PASSWORD'],
        database="mydb"
    )
    cur = db.cursor()
    cur.execute("SELECT * FROM bookings")
    data_to_read = cur.fetchall()
    db.close()
    rowarray_list = {}
    for row in data_to_read:
        t = row[1]
        if row[0] not in rowarray_list:
            rowarray_list[row[0]] = {}

        if row[1] not in rowarray_list[row[0]].keys():
            rowarray_list[row[0]][row[1]] = []
        
        rowarray_list[row[0]][row[1]].append(row[2])

    j = json.dumps(rowarray_list, indent = 4, sort_keys = True) 
    parsed = json.loads(j)
    return nice_json(parsed)


@app.route("/bookings/<username>", methods=['GET'])
def booking_record(username):
    db = mysql.connector.connect(
        host="127.0.0.1",
        user=os.environ['SECRET_USERNAME'],
        passwd=os.environ['SECRET_PASSWORD'],
        database="mydb"
    )
    cur = db.cursor()
    cur.execute('SELECT id FROM bookings WHERE id = "{}"'.format(username))
    data_to_read = cur.fetchall()
    if data_to_read == []:
        db.close()
        raise NotFound

    cur.execute('SELECT * FROM bookings WHERE id = "{}"'.format(username))
    data_to_read = cur.fetchall()
    db.close()
    rowarray_list = {}
    for row in data_to_read:
        t = row[2]
        if row[1] not in rowarray_list:
            rowarray_list[row[1]] = []

        rowarray_list[row[1]].append(t)

    j = json.dumps(rowarray_list, indent = 4, sort_keys = True) 
    parsed = json.loads(j)
    return nice_json(parsed)


if __name__ == "__main__":
    app.run(host= "0.0.0.0", port=8080, debug=True)

