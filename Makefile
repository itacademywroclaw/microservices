SERVICE = bookings movies showtimes users

build:
	@for item in $(SERVICE); do					\
		echo "Building $$item"; 				\
		docker build -t gcr.io/${PROJECT_ID}/$$item ./$$item;  	\
		docker push gcr.io/${PROJECT_ID}/$$item;		\
		echo "Done !";						\
	done;
